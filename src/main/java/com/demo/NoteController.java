package com.demo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "notes")
public class NoteController {

    private List<Note> notesList = new ArrayList<>();
    private Integer elements = 0;
    {
        for(int i = 0; i< 10000; i++) notesList.add(null);
        notesList.set(0,new Note(0, "Hello", "Hello contents"));
        notesList.set(1, new Note(1, "Todo", "Do something"));
        notesList.set(2, new Note(2, "Vue", "Add vuetify"));
        elements = 3;
    }

    @GetMapping()
    List<Note> getAll() {
        List<Note> newList = notesList.stream().filter(Objects::nonNull).collect(Collectors.toList());
        System.out.println(newList);
        Collections.reverse(newList);
        return newList;
    }

    @PostMapping()
    Note update(@RequestBody Note note) {
        if(note.getId() != null && elements > note.getId()) {
            notesList.set(note.getId(), note);
        } else {
            note.setId(elements);
            notesList.set(elements, note);
            elements++;
        }
        return note;
    }

    @GetMapping(value = "/{id}")
    Note getById(@PathVariable("id") final Integer id) {
        if(id <= elements) {
            return notesList.get(id);
        }
        return null;
    }

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable("id") final Integer id) {
        if(id <= elements) {
            notesList.set(id, null);
        }
    }
}
